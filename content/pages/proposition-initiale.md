+++
author = "Antoine"
title = "Proposition initiale"
date = "2020-11-30T20:30:00"
description = "Proposition de communication dans le cadre de l'appel à communications Études du livre au XXIe siècle : chantiers étudiants en cours."
bibfile = "data/bibliographie.json"
layout = "page"
+++
_Réponse à l'appel à communications "Études du livre au XXIe siècle : chantiers étudiants en cours" ([https://ex-situ.info/2020/10/etudes-du-livre-au-xxie-siecle-chantiers-etudiants-en-cours-appel/](https://ex-situ.info/2020/10/etudes-du-livre-au-xxie-siecle-chantiers-etudiants-en-cours-appel/))._

Les formes du livre n'ont probablement jamais été aussi multiples, un même texte pouvant désormais avoir plusieurs artéfacts différents, homothétiques ou non.
L'édition dans le vaste champ littéraire connaît aujourd'hui une forme d'hybridation {{< cite "ludovico_post-digital_2016" >}}.
Le livre a désormais des formes plurielles, parallèles ou complémentaires, imprimées ou numériques, auxquelles nous nous sommes plus ou moins habitués {{< cite "epron_ledition_2018" >}} : qu'il s'agisse d'un livre imprimé en offset, en impression à la demande, diffusé au format EPUB, disponible sous la forme d'un _livre web_ ou d'un jeu de données.
Il est désormais techniquement possible de générer presque automatiquement les fichiers nécessaires à ces différentes productions, grâce à des programmes informatiques.
Depuis une même source, le livre se déploie.
Le terme _déployer_ est utilisé dans le domaine du développement informatique pour signifier qu'un programme est mis en application. il peut s'agir autant d'un site web que d'un logiciel.
Ces nouvelles méthodes de fabrication {{< cite "flusser_petite_2002" 57 >}} du livre doivent être étudiées, afin de comprendre quelles influences elles peuvent avoir sur les processus d'écriture et de conception des textes {{< cite "audet_ecrire_2015" >}}.
Les scripts utilisés pour déployer les livres sont-ils les nouveaux gestes éditoriaux ?
Viennent-ils remplacer les mains qui relient les pages, les muscles qui serrent la presse typographique, les doigts sur le clavier qui activent les fonctions d'un logiciel ?
À travers un corpus de plusieurs initiatives éditoriales autant que programmatiques, nous nous proposons d'étudier ces modalités de production et de diffusion des livres, à la croisée des études du livre et des _critical code studies_ {{< cite "marino_critical_2020" >}}.

## Bibliographie

{{< bibliography >}}

## Notice bio-bibliographique
Antoine Fauchié est actuellement doctorant au Département des littératures de langue française à l'Université de Montréal sous la direction de Marcello Vitali-Rosati et Michael Sinatra, et responsable de projets à la Chaire de recherche du Canada sur les écritures numériques.
Après avoir accompagné les professionnels du livre en Rhône-Alpes en France sur les questions numériques pendant près de 10 ans, il réalise un travail de recherche sur les processus de publication et leur influence sur les pratiques d'écriture.
Avant de débuter une thèse à Montréal, Antoine Fauchié a développé une pratique professionnelle en tant que consultant indépendant en édition numérique, et en tant qu'enseignant à l'IUT2 de Grenoble en sciences de l'information.

Antoine Fauchié, antoine.fauchie@umontreal.ca, https://www.quaternum.net

{{< bibliography "data/antoine.json" >}}
