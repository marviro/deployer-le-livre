+++
author = "Antoine"
title = "De quoi déployer est-il le nom ?"
date = "2021-03-15"
description = "Déployer n'est pas un terme courant dans le domaine de l'édition, même numérique, il est donc nécessaire de comprendre le lien qui peut exister entre ce verbe transitif et le livre."
bibfile = "data/bibliographie.json"
weight = 2
+++
Les différentes définitions du terme _déployer_ s'accordent sur plusieurs caractéristiques communes&nbsp;: étendre, disperser, dérouler ou développer.
Qu'il s'agisse d'une action physique ou figurative, il y a _dépliement_ de quelque chose qui était là, mais partiellement ou totalement invisible.
Les ailes d'un oiseau, les parties d'une feuille pliée, les détails d'une idée, les éléments d'un plan&nbsp;: _déployer_ est un verbe transitif, il concerne _quelque chose_.
Une recherche conjointe des termes "livre" et "déploiement" nous entraîne dans un vaste océan de rapports bureaucratiques et autres documents administratifs éloignés d'un geste éditorial qui concernerait un _livre_.
Pourquoi alors parler de _déploiement_ pour le livre ?

Faisons un détour par un domaine où ce terme est largement exploité&nbsp;: l'informatique.
Celles et ceux qui sont habitué·e·s à côtoyer des programmes ou des lignes de code ont peut-être déjà aperçu la notion de déploiement et plus spécifiquement celle de _déploiement continu_.
Un programme informatique est une suite d'instructions exécutées par un ordinateur, le déploiement continu consiste à définir des règles pour cette exécution, cette mise en action.
L'objectif ici est d'automatiser le déclenchement d'un ou de plusieurs programmes, avec des paramètres, par exemple en spécifiant des conditions — si le programme A a fini de s'exécuter le programme B peut démarrer.
Le déploiement continu, ou intégration continue, a fait son apparition dans le développement informatique au début des années 2010 pour faciliter la production de programmes {{< cite shahin_continuous_2017 >}}.
Il y a évidemment un objectif mercantile derrière cette pratique — rapidité, reproductibilité, rentabilité —, mais c'est aussi un moyen de rendre plus confortable l'environnement de travail des développeurs et des développeuses, aussi appelé _developer experience_ {{< cite coyier_what_2020 >}}.
En poussant les modifications d'un programme informatique, une personne lance automatiquement un déploiement sur l'ordinateur qu'elle utilise ou sur un serveur à l'autre bout de la planète, que ce soit des tests, de la production de fichiers, une diffusion, etc.
Des plateformes accessibles au grand public permettent de mettre en pratique relativement facilement du déploiement continu, comme GitLab CI/CD (https://docs.gitlab.com/ee/ci/introduction/index.html) — quelques exemples seront présentés par la suite.
D'un certain point de vue le déploiement continu est une façon de déporter les scripts d'automatisation utilisés depuis longtemps dans le domaine informatique — de la même façon que l'informatique dans les nuages n'est que le fait de faire fonctionner certains services sur l'ordinateur d'un·e autre.

Le lien avec le livre se dessine peu à peu, il est aisé de deviner ce que peut apporter le déploiement continu à l'édition&nbsp;: activer des programmes capables de produire différents artefacts comme un fichier PDF destiné à l'impression, un fichier EPUB pour de la diffusion numérique, des documents XML pour des plateformes, etc.
Même en faisant abstraction des ressorts informatiques, décrire l'acte d'édition comme un déploiement est assez cohérent.
Activer la presse, déplier les feuilles d'impression, disperser les exemplaires d'un livre, les exemples ne manquent pas pour prouver que le livre se déploie depuis ses origines.
Nous parlons bien de conception, de fabrication, de production et de diffusion, dans l'objectif de rendre disponible un texte.
Cette idée du déclenchement d'actions en fonction des nécessités de production éditoriale raisonne avec la définition de l'édition et plus spécifiquement en tant que processus {{< cite epron_ledition_2018 6 >}}.
Deux des trois étapes ou fonctions de ce processus peuvent être régies par un système basé sur le déploiement continu&nbsp;: les fonctions de production et de diffusion comme évoqué précédemment.
Le prototypage des artéfacts, la fabrication du livre, la production des exemplaires, mais aussi la diffusion de ces objets — physiques ou numériques — via différents canaux, ainsi que celle de leurs métadonnées&nbsp;: il est possible de se dégager de manipulations répétitives via l'utilisation de protocoles éditoriaux traduits en commandes informatiques.
Les conditions d'existence d'une publication, ces "dynamiques" {{< cite vitali_rosati_quest-ce_2016 >}}, peuvent être énumérées dans un système basé sur le déploiement continu, et ainsi traduire autrement le geste éditorial {{< cite ouvry-vial_acte_2007 >}}.
En parallèle du processus d'édition considéré de façon générique, la question de l'_hybridation_ développée par Alessandro Ludovico {{< cite ludovico_post-digital_2016 >}} nourrit également le lien entre le _déploiement continu_ et le livre.
Comment co-existent ou s'articulent plusieurs expressions ou matérialisations d'un même livre&nbsp;?
Que ce soit une lettre d'information, un livre web, un fichier PDF en basse résolution, un livre imprimée à la demande ou encore une édition de luxe, il faut pouvoir produire différentes versions, peut-être à partir d'une même source.
En définissant des règles et des conditions, reposant sur des modèles distincts, la fabrication de ces artéfacts peut être facilitée — nous reviendrons sur cette question de facilitation qui ne peut être réduite à une recherche de rentabilité.
La condition de réalisation de l'hybridation pourrait être le déploiement continu.

Ces aspects de définition permettent d'envisager de façon théorique le déploiement continu dans le cas du livre.
Il faut observer comment se constituent ces différent rouages techniques, et notamment l'agencement des étapes du processus d'édition dans une perspective multimodale ou d'hybridation.
Nous pourrons ensuite lister les contraintes de telles pratiques pour ensuite découvrir les cas pratiques de plusieurs expérimentations éditoriales, l'occasion d'étudier les frictions avec le livre.
