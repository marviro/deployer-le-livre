+++
author = "Antoine"
title = "Dessine moi un déploiement continu"
date = "2021-03-17"
description = "Pour comprendre le fonctionnement du déploiement continu, décrivons et schématisons les étapes qui le constituent."
bibfile = "data/bibliographie.json"
weight = 3
+++
Le fonctionnement sommaire et générique se constitue en plusieurs étapes liées les unes aux autres, des espaces de travail qui permettent de guider un processus vers son accomplissement.
Ainsi le déploiement continu est souvent décrit en trois étapes successives comme indiqué dans la figure 1, le déclenchement de l'étape suivante dépendant du succès de la précédente&nbsp;:

1. une phase de test pour vérifier que la source (le _code_) répond aux attentes initiales et qu'il ne comporte pas d'erreurs&nbsp;;
2. une phase de production qui consiste à transformer les fichiers sources en un _produit_ utilisable, typiquement un logiciel exécutable ou un site web&nbsp;;
3. une phase de déploiement pour rendre disponible cette version de l'application — _logiciel_, _site web_ ou _application_ définissant le produit final.

{{< svg src="static/images/deployer-schema-generique.svg" caption="Figure 1&nbsp;: schéma du déploiement continu en trois étapes génériques." >}}

Cette description relativement abstraite peut être complétée par un cas pratique avec la figure 2 et l'exemple du _déploiement_ d'un site web&nbsp;:

1. une phase de test&nbsp;: vérification du balisage des pages HTML et de la présence d'informations diverses (métadonnées)&nbsp;;
2. une phase de production&nbsp;: assemblage des différents fichiers HTML avec les dépendances comme les feuilles de styles et les scripts pour les éléments interactifs&nbsp;;
3. une phase de déploiement&nbsp;: l'envoi des fichiers sur un serveur web qui servira le site web aux personnes qui demanderont l'accès.

{{< svg src="static/images/deployer-schema-site-web.svg" caption="Figure 2&nbsp;: schéma du déploiement continu d'un site web." >}}

Afin d'illustrer brièvement cette relation entre informatique et édition, nous pourrions prolonger cet exemple avec le cas d'une édition critique en <abbr title="Text Encoding Initiative">TEI</abbr>&nbsp;: la phase de test consisterait en la vérification des fichiers XML en rapport avec un schéma précis&nbsp;; la phase de production concernerait la conversion des fichiers XML vers des formats HTML via des feuilles de transformation en XSLT&nbsp;; la phase de déploiement enfin serait l'envoi des fichiers transformés sur un serveur web via un protocole de transfert comme <abbr title="Secure Shell">SSH</abbr>.

Avant d'explorer des expérimentations littéraires et éditoriales, des précisions sont nécessaires pour ne pas rester sur une apparente linéarité dans ce processus.
Tout d'abord ces étapes peuvent être plus nombreuses et les dépendances entre elles peuvent être plus complexes, les règles et les conditions peuvent s'entremêler et constituer ainsi un mécanisme aux conditions multiples.
Ensuite il faut ajouter une dimension supplémentaire.
Plusieurs environnements de déploiement, qui ont une même source, peuvent cohabiter et ainsi permettre d'envisager l'enchevêtrement de phases de travail.
Nous verrons dans une prochaine partie sur les conditions du déploiement continu que ces environnements se matérialisent sous la forme de _branches_.
Enfin il apparaît déjà ici une dimension bien plus large que celle de l'édition.
La formulation des règles d'exécution est une écriture {{< cite vitali-rosati_quest-ce_2020 >}}, au même titre que le texte qui pourrait être concerné par un tel processus de déploiement continu.

À la suite de cette tentative de schématisation du déploiement continu, nous allons explorer des initiatives éditoriales et littéraires, afin de constater comment se concrétise ce fonctionnement issu du domaine informatique.
