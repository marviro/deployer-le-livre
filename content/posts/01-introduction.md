+++
author = "Antoine"
title = "Introduction"
date = "2021-03-10"
description = "Déployer le livre. Voilà une invitation adressée à toute personne désireuse de comprendre comment un texte devient une publication via l'usage de processus issus du développement informatique. Nous nous proposons d'explorer la question de la fabrication du livre dans ses multiples expressions."
bibfile = "data/bibliographie.json"
weight = 1
+++
Les formes du livre n'ont probablement jamais été aussi multiples, un même texte pouvant désormais avoir plusieurs artéfacts différents, homothétiques ou non.
L'édition dans le vaste champ littéraire connaît aujourd'hui une forme d'hybridation {{< cite "ludovico_post-digital_2016" >}}.
Le livre a désormais des formes plurielles, parallèles ou complémentaires, imprimées ou numériques, auxquelles nous nous sommes plus ou moins habitué·e·s {{< cite "epron_ledition_2018" >}}&nbsp;: qu'il s'agisse d'un livre imprimé en offset, issu d'un processus d'impression à la demande, diffusé au format EPUB, disponible sous la forme d'un _livre web_ ou d'un jeu de données.
Il est désormais techniquement possible de générer presque automatiquement les fichiers nécessaires à ces différentes productions, grâce à des programmes informatiques.
Depuis une même source, le livre se déploie.
Ces pratiques d'édition numérique sont multiples, ces mises en œuvre méritent que nous nous y attardions, que nous les analysions.

Le terme _déployer_ est utilisé dans le domaine du développement informatique pour signifier qu'un programme est mis en application. Il peut s'agir autant d'un site web que d'un logiciel.
Les lignes de code s'activent selon des règles établies et dictées.
Ces nouvelles méthodes de fabrication {{< cite "flusser_petite_2002" 57 >}} du livre doivent être étudiées afin de comprendre quelles influences elles peuvent avoir sur les processus d'écriture et de conception des textes {{< cite "audet_ecrire_2015" >}}.
Il ne s'agit pas de la question de l'automatisation en soit, mais plutôt de celle de l'agencement des différentes étapes de ces déploiements et de la façon de les exprimer.
Notre intérêt se porte sur les conditions d'existence des artéfacts, reflet d'une certaine vision du monde.

Plusieurs questions en lien avec l'édition numérique tout autant que les pratiques littéraires peuvent être posées.
Les scripts utilisés pour déployer les livres sont-ils les nouveaux gestes éditoriaux&nbsp;?
Viennent-ils remplacer les mains qui relient les pages, les muscles qui serrent la presse typographique, les doigts sur le clavier qui activent les fonctions d'un logiciel, les algorithmes secrets des logiciels propriétaires&nbsp;?
À travers une analyse des questions littéraires, techniques et éditoriales, et avec l'aide de présentations d'initiatives éditoriales autant que programmatiques, nous nous proposons d'étudier ces modalités de production et de diffusion des livres, à la croisée des études du livre et des _critical code studies_ {{< cite "marino_critical_2020" >}}.

Cette _communication_ s'inscrit dans le colloque [Études du livre au XXIe siècle](https://projets.ex-situ.info/etudesdulivre21/) qui se déroule progressivement pendant les mois de mars à mai 2021.
Cette entreprise s'établit selon une recherche performative, les lignes que vous lisez sont elles aussi déployées, au fur et à mesure, à raison d'un nouvel épisode tous les jours ou tous les deux jours, entre le 10 et le 24 mars.
Pour suivre les mises en ligne successives des fragments rendez-vous sur [deployer.quaternum.net](https://deployer.quaternum.net).
