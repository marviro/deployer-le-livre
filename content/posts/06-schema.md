+++
author = "Antoine"
title = "Les étapes du déploiement"
date = "2021-03-29"
description = "Observons les étapes du déploiement dans le cas d'un livre numérique."
bibfile = "data/bibliographie.json"
weight = 6
+++
Si nous avons jusqu'ici donné quelques exemples du déploiement continu, revenons en détail sur les différentes étapes d'un processus pour fabriquer et produire un livre numérique.
Appliquées à un projet éditorial, les conditions présentées précédemment s'agencent et donnent forme à une chaîne d'édition.
Nous prenons le cas d'un livre numérique au format livre web {{< cite fauchie_livre_2017 >}}, ce format ou cette forme regroupant un certain nombre de caractéristiques pertinentes pour le mode de déploiement continu.

{{< svg src="static/images/deployer-schema-branches.svg" caption="Figure 4&nbsp;: schéma du déploiement continu pour le cas d'un livre numérique." >}}

Dans la figure 4 ci-dessus nous pouvons noter plusieurs points importants&nbsp;:

- le **dépôt du projet** contient tous les fichiers, organisés et versionnés, ces fichiers sont à plat, lisibles par des humains et des programmes informatiques&nbsp;;
- **plusieurs versions** d'un ensemble de fichiers peuvent cohabiter, pour plus de lisibilité ces versions sont nommées **branches** — ici _branche de développement_ et _branche de production_ —, la branche est un concept emprunté au logiciel de gestion de versions Git&nbsp;;
- à chaque **branche** correspond un environnement de déploiement&nbsp;: un ensemble d'instructions comprenant l'activation de programmes et le déplacement des fichiers produits — ces fichiers formant les artefacts&nbsp;;
- la différence entre ces deux **environnements de déploiement** est ici le serveur qui donne accès au résultat final&nbsp;: un serveur de développement comme espace d'édition intermédiaire ou temporaire, et un serveur de production comme espace de publication ou d'accès public finalisé&nbsp;;
- les instructions de déploiement peuvent diverger d'une branche à une autre, ouvrant des possibles pour produire des artefacts très différents avec une base néanmoins commune.

Nous ne pouvons pas préciser chaque brique technique issue des technologies de l'édition numérique {{< cite blanc_technologies_2018 >}}, mais celles qui sont convoquées ici peuvent être qualifiées d'ouvertes, de non conventionnelles ou encore de modulaires.
Un ensemble de solutions techniques qui sont orchestrées par ce processus qu'est le déploiement continu.
