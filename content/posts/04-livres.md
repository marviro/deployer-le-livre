+++
author = "Antoine"
title = "Des livres déployés"
date = "2021-03-19"
description = "Comment le déploiement continu peut être utilisé pour fabriquer des livres&nbsp;?"
bibfile = "data/bibliographie.json"
weight = 4
+++
[Comme nous l'avons vu](#module-3), le déplacement de l'usage du déploiement continu depuis l'informatique vers l'édition est un mouvement cohérent d'un point de vue théorique, et plus précisément pour la matérialisation des fonctions du processus éditorial, ou pour les conditions de réalisation de l'hybridation et les besoins de production d'artefacts.
Des implémentations pratiques peuvent également être observées, les premières initiatives viennent des domaines de la documentation technique et des manuels, la porosité entre intérêt technique et démarche éditoriale étant plus forte.
L'objectif est à la fois de donner un cadre de travail à des auteurs, autrices, éditeurs et éditrices qui ont une culture technique qui coïncide avec une reconfiguration des processus d'édition, mais également de chercher à automatiser facilement certaines tâches répétitives.
Les raisons pour lesquelles la majorité des structures d'édition n'adoptent pas ces pratiques peuvent être de deux types&nbsp;: les compétences techniques évoquées précédemment, et plus précisément celles nécessaires à la mise en œuvre de chaînes éditoriales basées sur le déploiement continu — ce point sera traité dans la partie suivante ; les outils habituellement utilisés, comme les traitements de texte et les logiciels de publication assisté par ordinateur, qui imposent un fonctionnement spécifique et notamment l'imperméabilité des étapes d'édition entre elles.

La démarche de Thomas Parisot, pour la publication de son livre _Node.js : apprendre par la pratique_ (https://oncletom.io/node.js/), est une illustration du rapprochement entre informatique et édition {{< cite parisot_repenser_2018 >}}, et du principe du déploiement continu.
À chaque modification d'un fichier source, le livre est généré en un format web disponible en ligne, ainsi qu'en des formats que peut utiliser l'éditeur pour fabriquer l'ouvrage avec des outils plus classiques — en l'occurrence un traitement de texte et un logiciel de publication assisté par ordinateur.
Chaque enregistrement déclenche une action, par l'intermédiaire de programmes — comme les quelques lignes le présentent ci-dessous.

```yaml
script:
- ./bin/cli.js --help
- npm test
- make build-html
- make build-odt
- xmllint --noout dist/book.fodt 2>&1
```
_Fragment du script permettant de générer plusieurs formats à partir d'une même source_

Pourquoi adopter une telle pratique&nbsp;?
L'auteur n'a qu'à paramétrer une fois la production des versions intermédiaires de son livre, versions qui facilitent ensuite les relectures.
L'éditeur peut obtenir plus facilement des formats adéquats utiles pour sa propre chaîne d'édition, sans que l'auteur n'ait à réaliser des opérations fastidieuses pour produire ou transmettre ces formats — parfois exigeants.
Ne nous y trompons pas, le domaine concerné se prête bien à ce type d'expérimentation, et les connaissances de Thomas Parisot lui ont permis d'envisager un tel fonctionnement.

Les éditions Abrüpt orientent certains de leurs projets littéraires vers le même type de pratique éditoriale.
Maison d'édition suisse consacrée à des auteurs et autrices contemporaines, ou à des rééditions de textes du vingtième siècle, Abrüpt propose des livres radicaux dans leurs propos et dans leurs formes.
Ces formes sont diverses, les antilivres _statiques_ côtoient ainsi les antilivres _dynamiques_ {{< cite fauchie_antilivre_2021 >}}, ces derniers pouvant être présentés comme des objets numériques non homothétiques.
Pour produire ces différentes formes des scénarios sont pensés et décrits, puis traduits par des scripts, des instructions écrites par des humains que des programmes vont appliquer.
Cela signifie que des règles sont établies pour transformer des fichiers, pour les assembler, afin de fabriquer des objets comme un site web, un fichier PDF pour l'impression ou encore un livre numérique au format EPUB.

{{< figure src="/images/abrupt-vent-douest.png" caption="Figure 3&nbsp;: capture d'écran de l'antilivre dynamique <em>Vent d’ouest</em> de Pier Lampás" >}}

Et il y a de la littérature jusque dans les lignes de code qui automatisent certaines manipulations, notamment les messages confirmant la conversion réussie de certains fichiers, par exemple "Vous contemplez le vide. Et le vide vous contemple." ([source](https://gitlab.com/antilivre/gitterature/-/blob/master/.gitlab-ci.yml)).
Si le déploiement continu tel que décrit jusqu'ici n'est pas totalement à l'œuvre pour les ouvrages d'Abrüpt, tous les composants sont présents.
Car le déploiement continu nécessite des conditions spécifiques, ce que nous devons détailler dans la prochaine partie.

Avant de décrire les conditions du déploiement continu, il faut mentionner ici deux points essentiels.
Le premier concerne les différents niveaux que porte l'expression "déployer le livre"&nbsp;: il peut s'agir d'actions simples comme le fait de mettre à disposition des fichiers, typiquement une diffusion numérique est déclenchée à chaque enregistrement, le dépôt de fichiers n'est donc plus fait manuellement sur des serveurs&nbsp;; des actions plus complexes comme la génération de fichiers via l'appel de programmes et l'application de modèles peuvent permettre la création de fichiers.
Le deuxième point est un avertissement&nbsp;: ces processus ne sont pas des pratiques secondaires par rapport aux tâches plus classiques d'édition.
Pourquoi cet avertissement&nbsp;?
Considérer ces questions sous l'angle de la seule technique en tant que _moyen_ serait une erreur.
De la même façon que le réglage des outils d'écriture des auteurs et autrices n'est pas un détail {{< cite bon_apres_2011 >}}, les rouages de ces déploiements est partie intégrante d'une performance littéraire {{< cite audet_ecrire_2015 >}}, d'une démarche d'écriture, d'édition, de _fabrication_ {{< cite flusser_petite_2002 >}}.
