+++
author = "Antoine"
title = "Les conditions du déploiement"
date = "2021-03-25"
description = "Choisir le déploiement continu pour fabriquer des livres implique des conditions précises."
bibfile = "data/bibliographie.json"
weight = 5
+++
Le déploiement continu est une chaîne de traitements automatisée ou semi-automatisée.
Appliquée à l'édition, le déploiement continu permet de concevoir et de produire des livres.
L'adoption de ce processus est pertinent dès qu'il y a plus d'une forme ou expression pour un même objet éditorial.
En d'autres termes le déploiement continu se prête bien à l'édition multimodale.
À plusieurs reprises les formes du livre ont cohabité, et cela depuis l'apparition du codex {{< cite manguel_histoire_1998 157 >}}&nbsp;: le codex aux côtés du volumen, les grands comme les petits formats, les qualités de papier ou d'impression, etc.
Le numérique permet désormais d'envisager un _déploiement_ des artefacts d'un même livre, et cela dès que nous considérons l'édition numérique comme autre chose qu'une reproduction des pratiques analogiques.
Il faut s'éloigner d'une _édition homothétique_ qui tente de reproduire, avec un ordinateur, et avec un certain échec, la beauté du geste des machines mécaniques.
Reconsidérer les pratiques d'édition — avec le numérique — requiert une reconfiguration profonde de nos pratiques mais également de notre rapport aux outils, aux logiciels et aux programmes que nous utilisons.

> Il est certain que la fabrique de l’avenir sera beaucoup plus souple que celle d’aujourd’hui, et tout aussi certain qu’elle donnera du rapport entre l’homme et l’outil une formulation entièrement renouvelée.  
> {{< cite flusser_petite_2002 60 >}}

Un processus dit de déploiement continu est conditionné par plusieurs contraintes.
La première condition est la façon d'écrire les fichiers source, les fragments qui composeront le livre.
D'habitude concentré dans des fichiers issus de traitements de texte (que ce soit des logiciels ou des services en ligne) ou de logiciels de publication assistée par ordinateur, le texte doit être décomposé.
Cela signifie que les fichiers puissent être lus et analysés facilement par des programmes, il faut qu'ils soient _à plat_, comme des fichiers dits plein texte.
Travailler avec des fichiers texte est une habitude dans certains domaines éditoriaux techniques, c'est une pratique encore rare dans la plupart des autres sphéres du livre.
Le rapport au texte et à sa matérialisation est renversé&nbsp;: plutôt que d'attribuer des caractéristiques graphiques au texte, il faut lui donner une sémantique, le structurer.
Le balisage est une façon d'attribuer du sens au texte tout en conservant une lisibilité à la fois du texte lui-même et de cette couche sémantique.
Qu'il soit léger, complexe ou normalisé, le balisage est une approche compréhensible (voir lisible pour le balisage léger) pour les humains, et pour les machines.
C'est à partir de ce balisage que les différentes formes du livre peuvent être produites, via des convertisseurs ou des générateurs qui se chargent de transformer les fichiers sources en formats PDF, EPUB, HTML, etc.

La deuxième condition est la gestion des fichiers, l'utilisation d'un système de gestion de versions ouvre des perspectives puissantes.
La gestion de versions consiste au suivi des fichiers qui composent un projet — un livre dans notre cas —, suivi qui se fait par plusieurs personnes de façons synchrone et asynchrone.
Git est un logiciel de gestion de versions dont des plateformes de partage de code comme GitLab ou GitHub ont permis une certaine popularité.
Git est basé sur plusieurs principes pour gérer du texte (et plus spécifiquement du code) et le publier, nous pouvons en citer quelques-uns&nbsp;: chaque série de modifications est identifiée et décrite dans un _commit_ qui contient une capture de l'ensemble des fichiers d'un projet, il est ainsi possible de naviguer dans l'historique du projet et de retrouver ses différentes composantes à n'importe quel moment&nbsp;; les _commits_ constituent un graphe de contributions qui peut être organisé en _branche_, offrant la possibilité de travailler à n'importe quel moment sur une version spécifique du projet&nbsp;; 
Des plateformes comme GitLab ou GitHub, ou des services tiers branchés sur celles-ci, permettent de réaliser des actions qui sont déclenchées à chaque modification, à chaque _commit_.
Ainsi des opérations sont réalisées de façon automatique, avec comme déclic une intervention sur un fichier, et qui plus est en prenant en compte une branche spécifique.
Plusieurs personnes interviennent 

D'autres modèles sont possibles, mais celui-ci offre suffisamment de souplesse {{< cite flusser_petite_2002 >}}, que ce soit du point de vue de la lisibilité, de l'interopérabilité, de la perméabilité, de l'évolutivité ou de la reproductibilité.
Sans détailler ces différents points, nous pouvons tout de même prendre un exemple, un projet de livre qui se base sur le déploiement continu implique plusieurs choses&nbsp;: l'utilisation de fichiers texte balisés, fichiers qui seront encore lisibles dans dix ou vingt ans&nbsp;; ces fichiers peuvent être utilisés quelque soit l'environnement de travail des personnes qui interviennent dessus&nbsp;; de nouvelles formes peuvent être produites sans se soucier du format des sources&nbsp;; un projet peut être dupliqué pour le détourner et utiliser tout ce qui est _autour_ du texte (modèle de structuration, mise en forme).
Par ailleurs ce modèle inspiré des pratiques de l'informatique offre la possibilité de profiter d'une communauté active et ouverte, habituée notamment à travailler en _agilité_ ou avec une approche d'amélioration progressive.

Ces conditions, qui sont aussi des contraintes, apportent d'autres avantages en dehors même du déploiement continu&nbsp;: elles donnent un cadre de travail et structurent les étapes d'édition&nbsp;; elles repositionnent notre rapport aux programmes et aux machines en formulant explicitement les instructions&nbsp;; elles sont aussi une description réflexive des intentions éditoriales, une "programmation lettrée" {{< cite knuth_literate_1984 >}} dans laquelle plusieurs niveaux de texte co-existent — le contenu, le balisage du contenu, les règles d'exécution pour la production.
Éditer est une démarche profondément technique, abstraction faite de considération numérique, il s'agit d'un _déploiement_ de la pensée.
Le déplacement des usages des logiciels classiques vers un ensemble de fichiers balisés et d'instructions sous forme de code est une autre expression de l'écriture comme technologie {{< cite bolter_writing_2001 >}}.
Plus encore, il s'agit d'une écriture complète et réflexive, le texte est autant décrit et structuré que la fabrique elle-même qui le porte.
Le texte, la fabrique et les artefacts sont donc trois phénomènes d'une même intention éditoriale.
